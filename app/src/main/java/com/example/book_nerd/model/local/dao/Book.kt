package com.example.book_nerd.model.local.dao

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "book_table")

data class Book (
    @PrimaryKey() val id: String,

    val title: String,
    val year: String
)