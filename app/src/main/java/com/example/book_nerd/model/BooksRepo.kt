package com.example.book_nerd.model

import com.example.book_nerd.model.remote.BooksService
import com.example.book_nerd.model.response.BooksDTO
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Named

class BooksRepo @Inject constructor(@Named("retrofit") private val service: BooksService) {

    suspend fun getBooks(): BooksDTO = withContext(Dispatchers.IO) {
        val listOfVar = service.getBooks()
        BooksDTO(listOfVar)
    }

}