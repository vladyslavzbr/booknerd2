package com.example.book_nerd.model.remote

import com.example.book_nerd.model.local.dao.Book
import com.example.book_nerd.model.response.BooksDTO
import retrofit2.http.GET

interface BooksService {

    @GET("books/100")
        suspend fun getBooks(): List<BooksDTO.Book>
}