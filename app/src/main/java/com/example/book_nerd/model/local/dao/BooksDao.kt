package com.example.book_nerd.model.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query


@Dao
interface BooksDao {

    @Query("SELECT * FROM book_table")
    suspend fun getAll(): List<Book>

    @Insert
    suspend fun insert(book: List<Book>)
}