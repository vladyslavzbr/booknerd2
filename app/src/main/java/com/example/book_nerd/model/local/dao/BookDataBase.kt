package com.example.book_nerd.model.local.dao

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase


@Database(entities = [Book::class], version = 3)
abstract class BookDatabase : RoomDatabase() {

    abstract fun booksDao(): BooksDao

    companion object {
        private const val DATABASE_NAME = "book.db"

        @Volatile
        private var instance: BookDatabase? = null

        fun getInstance(context: Context): BookDatabase {
            return instance ?: synchronized(this) {
                instance ?: buildDatabase(context).also { instance = it }
            }
        }

        private fun buildDatabase(context: Context): BookDatabase {
            return Room.databaseBuilder(context, BookDatabase::class.java, DATABASE_NAME).build()
        }
    }


}
