package com.example.book_nerd.model.response

data class BooksDTO(
    val books: List<Book>
) {
    data class Book(
        val id: String,
        val title: String,
        val year: String,

    )
}