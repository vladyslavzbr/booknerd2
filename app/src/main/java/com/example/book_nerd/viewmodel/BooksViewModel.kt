package com.example.book_nerd.viewmodel

import androidx.lifecycle.*
import com.example.book_nerd.model.BooksRepo
import com.example.book_nerd.model.response.BooksDTO
import com.example.book_nerd.view.BooksState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class BooksViewModel @Inject constructor(private val repo: BooksRepo) : ViewModel() {

    private val _state = MutableLiveData(BooksState())
    val state: LiveData<BooksState> get() = _state

    private val _stringList = MutableLiveData<BooksDTO>()
    val stringList: LiveData<BooksDTO> get() = _stringList

    init {
        viewModelScope.launch {
            val booksDTO = repo.getBooks()
            _state.value = BooksState(books = booksDTO.books)
        }
    }

    fun getBooks() {
        viewModelScope.launch {
            val book = repo.getBooks()
            _stringList.value = book
        }
    }

    class BooksViewModelFactory(
        private val booksRepo: BooksRepo
    ) : ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return BooksViewModel(booksRepo) as T
        }
    }
}