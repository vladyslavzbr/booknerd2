package com.example.book_nerd.di

import com.example.book_nerd.model.remote.BooksService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)

object BooksServiceModule  {

    private const val BASE_URL = "https://the-dune-api.herokuapp.com/"

    @Provides
    @Singleton
    @Named("retrofit")
    fun provideBookService(): BooksService {
        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        return retrofit.create(BooksService::class.java)
    }
}