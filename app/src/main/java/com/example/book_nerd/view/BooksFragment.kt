package com.example.book_nerd.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.book_nerd.R
import com.example.book_nerd.databinding.FragmentBooksBinding
import com.example.book_nerd.model.BooksRepo
import com.example.book_nerd.viewmodel.BooksViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


@AndroidEntryPoint
class BooksFragment : Fragment() {
    private var _binding: FragmentBooksBinding? = null
    private val binding get() = _binding!!

    @Inject lateinit var repo: BooksRepo
    private val booksViewModel by viewModels<BooksViewModel>() {
        BooksViewModel.BooksViewModelFactory(repo)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentBooksBinding.inflate(
        inflater, container, false
    ).also { _binding = it }.root

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        booksViewModel.state.observe(viewLifecycleOwner) {
            binding.tvBooks.text = it.books.toString()
        }

    }
}