package com.example.book_nerd.view

import com.example.book_nerd.model.response.BooksDTO

data class BooksState(
    val books: List<BooksDTO.Book> = emptyList()
)