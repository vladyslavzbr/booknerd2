package com.example.book_nerd

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp

class BookAplication: Application()